package com.camunda.fox.showcase.invoice.de.tasklist;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.FormService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.TaskService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.task.Task;

import com.camunda.fox.showcase.invoice.de.ProcessConstants;

@Named
@SessionScoped
public class TaskListController implements Serializable {

  private static final long serialVersionUID = 1L;

  @Inject
  private TaskService taskService;

  @Inject
  private FormService formService;

  @Inject
  private IdentityService identityService;

  private String currentUser = "kermit";

  private String currentGroup;

  private String currentDemoProcess = ProcessConstants.PROCESS_NAME_FOX;

  public long getPersonalTaskListCount() {
    return taskService.createTaskQuery().processDefinitionKey(currentDemoProcess).taskAssignee(currentUser).count();
  }

  public long getGroupTaskListCount(String role) {
    return taskService.createTaskQuery().processDefinitionKey(currentDemoProcess).taskCandidateGroup(role).count();
  }

  public List<Task> getTaskList() {
    if (currentGroup == null || currentGroup.length() == 0) {
      return taskService.createTaskQuery().processDefinitionKey(currentDemoProcess).taskAssignee(currentUser).list();
    } else {
      return taskService.createTaskQuery().processDefinitionKey(currentDemoProcess).taskCandidateGroup(currentGroup).list();
    }
  }

  public boolean isHasTasks() {
    if (currentGroup == null || currentGroup.length() == 0) {
      if (taskService.createTaskQuery().processDefinitionKey(currentDemoProcess).taskAssignee(currentUser).count() == 0) {
        return false;
      } else {
        return true;
      }
    } else {
      if (taskService.createTaskQuery().processDefinitionKey(currentDemoProcess).taskCandidateGroup(currentGroup).count() == 0) {
        return false;
      } else {
        return true;
      }
    }
  }

  public List<String> getCurrentUserInfoKeys() {
    return identityService.getUserInfoKeys(currentUser);
  }

  public String getCurrentUserInfo(String key) {
    return identityService.getUserInfo(currentUser, key);
  }

  public List<User> getUsersList() {
    return identityService.createUserQuery().list();
  }
  
  public List<String> getDemoProcessesList() {
    ArrayList<String> list = new ArrayList<String>();
    list.add(ProcessConstants.PROCESS_NAME_FOX);
    list.add(ProcessConstants.PROCESS_NAME_ADONIS);
    return list;
  }
  
  public List<Group> getGroupsList() {
    return identityService.createGroupQuery().groupMember(currentUser).list();
  }

  public void setCurrentGroup(String currentGroup) {
    this.currentGroup = currentGroup;
  }

  public String getCurrentGroup() {
    return currentGroup;
  }

  public String getFormKey(Task task) {
    return formService.getTaskFormData(task.getId()).getFormKey();
  }

  public void claim(Task task) {
    taskService.claim(task.getId(), currentUser);
  }

  public void setCurrentUser(String currentUser) {
    this.currentUser = currentUser;
  }

  public String getCurrentUser() {
    return currentUser;
  }

  
  public String getCurrentDemoProcess() {
    return currentDemoProcess;
  }

  
  public void setCurrentDemoProcess(String currentDemoProcess) {
    this.currentDemoProcess = currentDemoProcess;
  }
}
