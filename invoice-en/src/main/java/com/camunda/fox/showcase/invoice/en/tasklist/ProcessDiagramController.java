package com.camunda.fox.showcase.invoice.en.tasklist;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.cdi.BusinessProcess;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.repository.DiagramLayout;
import org.activiti.engine.repository.DiagramNode;
import org.activiti.engine.runtime.ProcessInstance;


@Named
public class ProcessDiagramController {

  @Inject
  private RuntimeService runtimeService;

  @Inject
  private RepositoryService repositoryService;

  @Inject
  private HistoryService historyService;

  @Inject
  private BusinessProcess businessProcess;

  private ProcessInstance getCurrentProcessInstance() {
    return businessProcess.getProcessInstance();
  }

  public List<PositionedHistoricActivityInstance> getTraversedFlowNodes() {
    ArrayList<PositionedHistoricActivityInstance> alist = new ArrayList<PositionedHistoricActivityInstance>();

    ProcessInstance processInstance = getCurrentProcessInstance();

    if (processInstance != null) {
      DiagramLayout processDiagramLayout = repositoryService.getProcessDiagramLayout(processInstance.getProcessDefinitionId());

      List<HistoricActivityInstance> hlist = historyService.createHistoricActivityInstanceQuery().processInstanceId(processInstance.getId()).list();

      for (HistoricActivityInstance hact : hlist) {
        if (hact.getEndTime() != null) {
          PositionedHistoricActivityInstance pact = new PositionedHistoricActivityInstance(hact, processDiagramLayout.getNode(hact.getActivityId()));
          alist.add(pact);
        }
      }
    }

    return alist;
  }

  public List<DiagramNode> getActiveActivityBoundsOfLatestProcessInstance() {
    ArrayList<DiagramNode> list = new ArrayList<DiagramNode>();
    ProcessInstance processInstance = getCurrentProcessInstance();
    if (processInstance != null) {
      DiagramLayout processDiagramLayout = repositoryService.getProcessDiagramLayout(processInstance.getProcessDefinitionId());
      List<String> activeActivityIds = runtimeService.getActiveActivityIds(processInstance.getId());
      for (String activeActivityId : activeActivityIds) {
        list.add(processDiagramLayout.getNode(activeActivityId));
      }
    }
    return list;
  }

}
