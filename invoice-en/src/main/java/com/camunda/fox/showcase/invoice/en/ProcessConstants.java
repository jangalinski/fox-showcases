package com.camunda.fox.showcase.invoice.en;


public class ProcessConstants {

  public static final String PROCESS_NAME = "invoice-en";
  public static final String VARIABLE_INVOICE = "invoice";

}
