package com.camunda.fox.showcase.invoice.en.tasklist;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.FormService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.TaskService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.task.Task;

import com.camunda.fox.showcase.invoice.en.ProcessConstants;

@Named
@SessionScoped
public class TaskListController implements Serializable {

  private static final long serialVersionUID = 1L;

  @Inject
  private TaskService taskService;

  @Inject
  private FormService formService;

  @Inject
  private IdentityService identityService;

  private String currentUser = "kermit";

  private String currentGroup;

  public long getPersonalTaskListCount() {
    return taskService.createTaskQuery().processDefinitionKey(ProcessConstants.PROCESS_NAME).taskAssignee(currentUser).count();
  }

  public long getGroupTaskListCount(String role) {
    return taskService.createTaskQuery().processDefinitionKey(ProcessConstants.PROCESS_NAME).taskCandidateGroup(role).count();
  }

  public List<Task> getTaskList() {
    if (currentGroup == null || currentGroup.length() == 0) {
      return taskService.createTaskQuery().processDefinitionKey(ProcessConstants.PROCESS_NAME).taskAssignee(currentUser).list();
    } else {
      return taskService.createTaskQuery().processDefinitionKey(ProcessConstants.PROCESS_NAME).taskCandidateGroup(currentGroup).list();
    }
  }

  public boolean isHasTasks() {
    if (currentGroup == null || currentGroup.length() == 0) {
      if (taskService.createTaskQuery().processDefinitionKey(ProcessConstants.PROCESS_NAME).taskAssignee(currentUser).count() == 0) {
        return false;
      } else {
        return true;
      }
    } else {
      if (taskService.createTaskQuery().processDefinitionKey(ProcessConstants.PROCESS_NAME).taskCandidateGroup(currentGroup).count() == 0) {
        return false;
      } else {
        return true;
      }
    }
  }

  public List<String> getCurrentUserInfoKeys() {
    return identityService.getUserInfoKeys(currentUser);
  }

  public String getCurrentUserInfo(String key) {
    return identityService.getUserInfo(currentUser, key);
  }

  public List<User> getUsersList() {
    return identityService.createUserQuery().list();
  }

  public List<Group> getGroupsList() {
    return identityService.createGroupQuery().groupMember(currentUser).list();
  }

  public void setCurrentGroup(String currentGroup) {
    this.currentGroup = currentGroup;
  }

  public String getCurrentGroup() {
    return currentGroup;
  }

  public String getFormKey(Task task) {
    return formService.getTaskFormData(task.getId()).getFormKey();
  }

  public void claim(Task task) {
    taskService.claim(task.getId(), currentUser);
  }

  public void setCurrentUser(String currentUser) {
    this.currentUser = currentUser;
  }

  public String getCurrentUser() {
    return currentUser;
  }
}
