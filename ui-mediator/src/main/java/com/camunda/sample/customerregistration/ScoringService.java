package com.camunda.sample.customerregistration;

import java.util.UUID;

import javax.ejb.Stateless;
import javax.inject.Named;


@Named("scoringService")
@Stateless
public class ScoringService {
  
  public ScoreResult getScoringForCustomer(final Customer c) {    
    ScoreResult result = new ScoreResult();
    result.setAsynchronousAnswer(false);
    
    // Scoring for Berlin depends heavily on the exact area you live in :-)
    if ("Berlin".equals(c.getCity())) {
      String correlationKey = UUID.randomUUID().toString();
      result.setAsynchronousAnswer(true);
      result.setAsynchronousCorrelationKey(correlationKey);      
    }
    // Stuttgart is always poshy and gets a good score
    else if ("Stuttgart".equals(c.getCity())) {
      result.setScoringPoints(100l);
    }
    // and everybody else is considered trustworthy enough
    else {
      result.setScoringPoints(50l);
    }
    
    return result;    
  }
}
