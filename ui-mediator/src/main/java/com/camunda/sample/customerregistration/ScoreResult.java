package com.camunda.sample.customerregistration;

import java.io.Serializable;


public class ScoreResult implements Serializable {

  private static final long serialVersionUID = 1L;
  
  /**
   * set to true if the answer is sent later
   */
  private boolean asynchronousAnswer = false;
  
  private String asynchronousCorrelationKey = null;
  
  /**
   * The scoring result itself if provided
   */
  private Long scoringPoints = null;

  
  public boolean isAsynchronousAnswer() {
    return asynchronousAnswer;
  }

  
  public void setAsynchronousAnswer(boolean asynchronousAnswer) {
    this.asynchronousAnswer = asynchronousAnswer;
  }

  
  public String getAsynchronousCorrelationKey() {
    return asynchronousCorrelationKey;
  }

  
  public void setAsynchronousCorrelationKey(String asynchronousCorrelationKey) {
    this.asynchronousCorrelationKey = asynchronousCorrelationKey;
  }

  
  public Long getScoringPoints() {
    return scoringPoints;
  }

  
  public void setScoringPoints(Long scoringPoints) {
    this.scoringPoints = scoringPoints;
  }

}
