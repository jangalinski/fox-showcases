package com.camunda.sample.customerregistration;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.Connection;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueConnectionFactory;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;

@Named
@Stateless
public class CallbackService {

  @Inject
  private RuntimeService runtimeService;
  
  @PersistenceContext
  private EntityManager entityManager;
  
  @Resource(mappedName = "java:/queue/test")
  private Queue queue;

  @Resource(mappedName = "java:/JmsXA")
  private QueueConnectionFactory connectionFactory;  
  
  public void receiveCallback(String correlationKey) {
    // Execution execution = runtimeService.createExecutionQuery().variableValueEquals("scoreResultCorrelationKey", correlationKey).singleResult();
    // This does not work because of limitation in Activiti, do workaround for http://jira.codehaus.org/browse/ACT-1106 instead:
    ProcessInstance pi = runtimeService.createProcessInstanceQuery().variableValueEquals("scoreResultCorrelationKey", correlationKey).singleResult();
    Execution execution = runtimeService.createExecutionQuery().activityId("wait_for_scoring_result").processInstanceId(pi.getId()).singleResult();
    
    System.out.println("do callback on execution " + execution.getId());

    Long customerId = (Long) runtimeService.getVariable(execution.getProcessInstanceId(), CustomerService.VARNAME_CUSTOMER_ID);
    Customer customer = entityManager.find(Customer.class, customerId);    
    // but in the end Berlin is not that bad at all ;-)
    customer.setScoringPoints(99l);
    
    runtimeService.signal(execution.getId());  
  }
  
  public void triggerAsynchronousCallback(Customer customer, String asynchronousCorrelationKey) {
    try {
      Connection connection = connectionFactory.createConnection();
      Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
      MessageProducer producer = session.createProducer(queue);
      TextMessage message = session.createTextMessage(asynchronousCorrelationKey);
      producer.send(message);
      producer.close();
      session.close();
      connection.close();
    }
    catch (Exception ex) {
      throw new RuntimeException("Could not send JMS message", ex);
    }
  }

}
